# Completion for libncarg-bin executables

complete -W "-ngmath -sungks -ncarbd -ngmathbd -smooth -super -quick -agupwrtx -conransmooth -conranquick -conransuper -conrecsmooth -conrecquick -conrecsuper -dashsmooth -dashquick -dashsuper -dashchar -ictrans -noX11 -nox11" ncargcc
complete -W "-ngmath -sungks -ncarbd -ngmathbd -smooth -super -quick -agupwrtx -conransmooth -conranquick -conransuper -conrecsmooth -conrecquick -conrecsuper -dashsmooth -dashquick -dashsuper -dashchar -ictrans -noX11 -nox11" ncargf77
complete -W "-ngmath -sungks -ncarbd -ngmathbd -smooth -super -quick -agupwrtx -conransmooth -conranquick -conransuper -conrecsmooth -conrecquick -conrecsuper -dashsmooth -dashquick -dashsuper -dashchar -ictrans -noX11 -nox11" ncargf90

# ncargex
# " To invoke various classes of examples:"
#echo "   [-A] [-E] [-F] [-P] [-T] [-U] [-ngmath] [-class] [-ps] [-x11]"
#echo ""
#echo " To invoke various utilities:"
#echo "   [-areas] [-autograph] [-bivar] [-colconv] [-conpack]     "
#echo "   [-conpackt] [-conran_family] [-conrec_family] [-csagrid] "
#echo "   [-cssgrid] [-dashline] [-dashpack] [-dsgrid] [-ezmap]    "
#echo "   [-field_flow] [-fitgrid] [-gflash] [-gks] [-gridall]     "
#echo "   [-halftone] [-histogram] [-isosrfhr] [-isosurface]       "
#echo "   [-labelbar] [-natgrid] [-ngmisc] [-plotchar]             "
#echo "   [-polypack] [-pwrite_family] [-scrolled_title]           "
#echo "   [-seter] [-shgrid] [-softfill] [-spps] [-streamlines]    "
#echo "   [-surface] [-threed] [-vaspackt][-vectors] [-wmap]       "
#echo "   [-misc]                                                  "
#echo ""
#echo " Other options:"
#echo "   [-W workstation_type] [-n] [-clean] [-onebyone] names"
##echo ""


